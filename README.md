# GuessWhoC#
- Rebuild of A-Level project utilising new skills gained throughout univeristy.
- A re-creation of the game 'Guess Who' written in C# where the user (aka player) competes against the computer.

#### LINUX
**Build**<br>
`mcs /reference:System.Drawing /reference:System.Windows.Forms -out:GuessWho.exe src/*.cs`<br>
**Run**<br>
`mono GuessWho.exe`

#### WINDOWS
**Build**<br>
`csc src/*.cs`<br>
**Run**<br>
`./GuessWho.exe`
