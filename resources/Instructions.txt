Hello and welcome to Guess Who, here you will find the instructions to play the
game.

Firstly, choose a character and the computer will choose theirs. It will then be
randomly generated who goes first, you will be notified and prompted when it is
your turn to guess or answer a question. When asking, a question choose one from
the combo box and select confirm when you’re sure that is the question you want
to ask. When answering questions, check against your chosen character and select
yes or no. Do not lie, if you lie the computer will know in the end and you will
be disqualified and lose.

When you finally choose to guess the computers character, select
“Guess character” in the combo box and confirm, then wait to be prompted to
choose one of the characters on the board to guess, if you guess incorrectly you
will be notified and the game will continue. However, if you guess correctly the
game will finish, and you will be prompted to play again.

In Addition, if the computer guesses then just like answering a question, select
yes or no, if you select yes and they have guessed correctly then just like
stated above you will be prompted to play again.
