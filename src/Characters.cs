﻿using System;

namespace GuessWho
{
    public class Characters
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public bool CouldBePlayerChar = true;
        public bool CouldBeCompChar = true;
        public Features Feature;

        public Characters(Random RandomNumber, bool FloatingCharacters, string[] MaleNames, string[] FemaleNames)
        {
            if (RandomNumber.Next(0, 2) == 0) //generate the gender of a character
            {
                Gender = "Male";
            }
            else
            {
                Gender = "Female";
            }
            if(FloatingCharacters != true)//assign a name to a character if it is to be assigned to a board
            {
                Names(RandomNumber, Gender, MaleNames, FemaleNames);
            }
            else
            {
                Name = null;
            }
            Feature = new Features(RandomNumber, Gender);
        }

        public void Names(Random RandomNumber, string Gender, string[] MaleNames, string[] FemaleNames)
        {
            int Names = RandomNumber.Next(0, 31);

            if (Gender == "Male")
            {
                while (MaleNames[Names] == "")//Makes sure two characters cannot have the same name
                {
                    if (Names == MaleNames.Length - 1)
                    {
                        Names = 0;
                    }
                    else
                    {
                        Names++;
                    }
                }
                Name = MaleNames[Names];
                MaleNames[Names] = "";

            }
            else
            {
                while (FemaleNames[Names] == "")
                {
                    if (Names == FemaleNames.Length - 1)
                    {
                        Names = 0;
                    }
                    else
                    {
                        Names++;
                    }
                }
                Name = FemaleNames[Names];
                FemaleNames[Names] = "";
            }
        }
    }
}
