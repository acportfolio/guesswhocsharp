﻿using System;

namespace GuessWho
{
    public class Features
    {
        public string EyeColour { get; set; }
        public string FacialHair { get; set; }
        public string Accessory { get; set; }
        public string SkinColour { get; set; }
        public string SkinFeature { get; set; }
        public Hairs Hair;

        public Features(Random RandomNumber, string Gender)
        {
            string[] EyeColours = { "Brown Eyes", "Blue Eyes", "Green Eyes" };
            string[] FacialHairs = { "Beard", "Moustache", "No Facial Hair", "Stubble" };
            string[] Accessories = { "Glasses", "No Accessories", "Eye Patch", "Monocle", "Hat" };
            string[] SkinColours = { "White Skin", "Black Skin", "Brown Skin", "Olive Skin" };
            string[] SkinFeatures = { "Freckles", "Mole", "No Skin Features" };

            if (Gender == "Male")
            {
                FacialHair = FacialHairs[RandomNumber.Next(0, FacialHairs.Length)];
            }
            else
            {
                FacialHair = "No Facial Hair";
            }
            EyeColour = EyeColours[RandomNumber.Next(0, EyeColours.Length)];
            Accessory = Accessories[RandomNumber.Next(0, Accessories.Length)];
            SkinColour = SkinColours[RandomNumber.Next(0, SkinColours.Length)];
            SkinFeature = SkinFeatures[RandomNumber.Next(0, SkinFeatures.Length)];

            Hair = new Hairs(RandomNumber);
            while (Accessory == "Hat" && Hair.Length != "Bald")
            {
                Accessory = Accessories[RandomNumber.Next(0, Accessories.Length)];
            }
        }
    }
}
