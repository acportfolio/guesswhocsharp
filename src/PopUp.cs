using System.Drawing;
using System.Windows.Forms;

namespace GuessWho
{
    public class PopUp : Form
    {
        public PopUp(Label announcement)
        {
          this.StartPosition = FormStartPosition.CenterScreen;
          this.Name = "PopUp";
          this.Text = "PopUp";
          this.FormBorderStyle = FormBorderStyle.FixedSingle;
          this.BackColor = Color.LightSkyBlue;
          Controls.Add(announcement);
        }
    }
}
