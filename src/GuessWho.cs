using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Media;
using System.Threading;
using System.IO;

namespace GuessWho
{
    public class mainStage : Form
    {

      const int width = 1350;
      const int height = 755;

      int charAmount;
      int xAmount;
      int yAmount;
      const int xBoardDimension = 900;
      const int yBoardDimension = 480;
      char playerCharacterChosen;

      public struct FeatureInfo //defining of my user defined data type
      {
          public string feature;
          public int count;
      }

      public static void Main()
      {
        Application.Run(new mainStage());
      }

      public void FlowLayoutPanelStyle(FlowLayoutPanel f) //everything should be in these!!!
      {
          f.FlowDirection = FlowDirection.TopDown;
          f.Location = new Point((width / 2) - (f.Width / 2), (height / 2) - (f.Height / 2));
          Controls.Add(f);
      }

      public void AddtoFlowLay(FlowLayoutPanel f, Control item)
      {
        item.Width = f.Width - 5;
        item.Height = (f.Height / 2) - 7;
        f.Controls.Add(item);
      }

      public void LabelScale(Label temp, int x, int y)
      {
        temp.Size = new Size(temp.PreferredWidth, temp.PreferredHeight);
        temp.Location = new Point((x / 2) - (temp.PreferredWidth / 2) , y);
      }

      public void ComboScale(ComboBox temp, int width, int height, int x, int y)
      {
        temp.DropDownStyle = ComboBoxStyle.DropDownList;
        temp.Size = new Size(width, height);
        temp.Location = new Point(x, y);
      }

      public void ControlStyle(Control temp, string name, Color fColour, Color bColour, int fontNo)
      {
        if(name != "")
        {
          temp.Text = name;
          temp.TextAlign = ContentAlignment.MiddleCenter;
        }
        temp.ForeColor = fColour;
        temp.BackColor = bColour;
        temp.Font = new Font(temp.Font.FontFamily, fontNo);
        Controls.Add(temp);
      }


      public mainStage() //This sets up the size of my display and my user control
      {
        this.Size = new Size(width, height); //stop being able to change that
        this.FormBorderStyle = FormBorderStyle.FixedSingle;
        this.Name = "mainStage";
        this.Text = "GuessWho";
        this.BackColor = Color.DodgerBlue;
        mainMenu();
      }

      public void mainMenu()
      {
        Button playGame = new Button();
        Button instructions = new Button();
        Label title = new Label();
        ControlStyle(title, "Guess Who", Color.White, Color.Transparent, 40);
        LabelScale(title, width, 0);
        MenuControlsSetUp(playGame, instructions);
        FloatingCharacters(); //This calls the background animation of my game
      }

      public void MenuControlsSetUp(Button playGame, Button instructions)//This designs and displays the controls of the main menu of my game and sets up any event handlers
      {
        FlowLayoutPanel f = new FlowLayoutPanel();
        FlowLayoutPanelStyle(f);

        ControlStyle(playGame, "PlayGame", Color.White, Color.LightSkyBlue, 20);
        ControlStyle(instructions, "Instructions", Color.White, Color.LightSkyBlue, 20);

        AddtoFlowLay(f, playGame);
        AddtoFlowLay(f, instructions);

        playGame.Click += (sender, EventArgs) => { LandscapeSize(sender, EventArgs, playGame, instructions, f); };
        instructions.Click += new EventHandler(InstructionsPage);
      }

      public void InstructionsPage(object o, EventArgs e)
      {
        Label announcement = new Label();
        ControlStyle(announcement, File.ReadAllText("resources/Instructions.txt"), Color.White, Color.Transparent, 12);
        LabelScale(announcement, 775, 20);
        PopUp instructPop = new PopUp(announcement);
        instructPop.Size = new Size(775, announcement.Height + 60);
        instructPop.Show();
      }

      public void FloatingCharacters() //This is the background animation of my game showing the floating heads of characters as my client requested
      {
          Random randomNumber = new Random();
          const int amount = 40;
          bool floatingCharacters = true;
          Characters[] floating = GeneratingCharacters(randomNumber, amount, floatingCharacters); //This calls a function that will generate an array of characters
          Image[] charImages = CreateImages(floating, amount); //This function creates the images for the objects
          int objectX = 0;
          int objectY = 0;
          int objectVx = 0;
          int objectVy = 0;
          int[] objectXPos = new int[amount];
          int[] objectXVel = new int[amount];
          int[] objectYPos = new int[amount];
          int[] objectYVel = new int[amount];
          int[] objectRadius = new int[amount];

          for (int objects = 0; objects < amount; objects++)
          {
              objectRadius[objects] = randomNumber.Next(80, 200);
              objectVx = randomNumber.Next(1, 20);
              objectVy = randomNumber.Next(1, 20);
              objectX = randomNumber.Next(0, width - objectRadius[objects]);
              objectY = randomNumber.Next(0, height - objectRadius[objects]);
              objectXPos[objects] = objectX;
              objectYPos[objects] = objectY;
              objectXVel[objects] = objectVx;
              objectYVel[objects] = objectVy;
          }

          System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
          t.Interval = 1;
          t.Tick += (sender, EventArgs) => { TimerTick(sender, EventArgs, ref objectX, ref objectY, ref objectVx, ref objectVy, objectRadius, amount, ref objectXPos, ref objectYPos, ref objectXVel, ref objectYVel); }; //This is an event handler that calls a subroutine that will update the screen of the heads movement every time the timer ticks
          t.Start();

          Paint += (sender, PaintEventArgs) => { PaintBall(sender, PaintEventArgs, objectRadius, objectXPos, objectYPos, amount, charImages); }; //Draws the objects which the character's images are loaded onto
          DoubleBuffered = true;
      }

      public void TimerTick(object sender, EventArgs e, ref int objectX, ref int objectY, ref int objectVx, ref int objectVy, int[] objectRadius, int numberOfObjects, ref int[] objectXPos, ref int[] objectYPos, ref int[] objectXVel, ref int[] objectYVel)
      {
          for (int objects = 0; objects < numberOfObjects; objects++)
          {
              objectXPos[objects] += objectXVel[objects];
              if (objectXPos[objects] < 0 || objectXPos[objects] + objectRadius[objects] > ClientSize.Width) //Checks to see if the object is reaching the bounds of the display screen and if it is change the direction of the objects velocity
              {
                  objectXVel[objects] = -objectXVel[objects];
              }

              objectYPos[objects] += objectYVel[objects];
              if (objectYPos[objects] < 0 || objectYPos[objects] + objectRadius[objects] > ClientSize.Height)
              {
                  objectYVel[objects] = -objectYVel[objects];
              }
          }
          Refresh();
      }

      public void PaintBall(object sender, PaintEventArgs e, int[] objectRadius, int[] objectXPos, int[] objectYPos, int numberOfObject, Image[] charImages)//Draws the floating heads graphics and uploads the images
      {
          Image character;
          for (int objects = 0; objects < numberOfObject; objects++)
          {
              character = new Bitmap(charImages[objects]);
              e.Graphics.DrawImage(character, objectXPos[objects], objectYPos[objects], objectRadius[objects], objectRadius[objects]);
          }
      }

      public Image[] CreateImages(Characters[] allCharacters, int amount)//Loads the current feature images and layers them to create images of characters
      {
          Image[] images = new Image[amount];
          for (int chars = 0; chars < allCharacters.Length; chars++)
          {
              Bitmap character = new Bitmap(170, 170);
              Graphics g = Graphics.FromImage(character);
              string[] charactersFeatures = SetUpFeatureCatagories(allCharacters[chars]);
              Image[] charImage = new Image[charactersFeatures.Length - 3];
              int imgPos = 0;
              for (int feature = 2; feature < charactersFeatures.Length - 1; feature++)
              {
                  if (feature < 7 && charactersFeatures[feature] != "No Facial Hair" && charactersFeatures[feature] != "No Accessories" && charactersFeatures[feature] != "No Skin Features")
                  {
                      charImage[imgPos] = Image.FromFile("resources/" + charactersFeatures[feature] + ".png");
                  }
                  else if (feature == 7 && charactersFeatures[feature] != "Bald")
                  {
                      charImage[imgPos] = Image.FromFile("resources/" + charactersFeatures[feature + 1] + charactersFeatures[feature] + ".png");
                  }
                  imgPos++;
              }
              for (int imgPosNew = 0; imgPosNew < charactersFeatures.Length - 3; imgPosNew++)
              {
                  if (charImage[imgPosNew] != null)
                  {
                      g.DrawImage(charImage[imgPosNew], new Point(0, 0)); //Converts the layered images into a whole image
                  }
              }
              images[chars] = character; //Puts each character image into an array
          }
          return images;
      }

      public Characters[] GeneratingCharacters(Random randomNumber, int amount, bool floatingCharacters)//This function returns an array of instances of the character class
      {
          Characters[] allCharacters = new Characters[amount];
          string tempMale = "";
          int tempCharNumber;

          string[] maleNames = { "Tom", "Stewart", "Druitt", "Jack", "Leo", "Henry", "Joel", "Danny", "Michael", "Neil", "Clark", "Steve", "Stephen", "Will", "Biggy", "Nikola", "Mike", "Daniel", "Gavin", "Aaron", "Adam", "Charlie", "Ian", "George", "Nigel", "Gregory", "Joey", "Luke", "Matt", "Harry", "Kirk", "Mick" };
          string[] femaleNames = { "Kaylee", "Catarina", "Lucy", "Amanda", "Laura", "Cat", "Kara", "Alex", "Astra", "Calista", "Helen", "Sam", "Peggy", "Ashley", "Beca", "Elizabeth", "Janet", "Kate", "Bernie", "Serena", "Catherine", "Hannah", "Afina", "Chloe", "Abi", "Poppy", "Daisy", "Lily", "Dotty", "Kendal", "Sharon", "Jane" };

          for (int charNo = 0; charNo < allCharacters.Length; charNo++)
          {
              allCharacters[charNo] = new Characters(randomNumber, floatingCharacters, maleNames, femaleNames);//Here a character is created and assigned to an array position
              if (charNo > 0) //This character is the compared to previsously generetaed characters
              {
                  tempCharNumber = charNo - 1;
                  while (tempCharNumber > 0)
                  {
                      int count = 0;
                      string[] character1Check = SetUpFeatureCatagories(allCharacters[charNo]);
                      string[] character2Check = SetUpFeatureCatagories(allCharacters[tempCharNumber]);
                      for (int i = 2; i < character1Check.Length; i++)
                      {
                          if (character1Check[i] == character2Check[i])
                          {
                              count++;
                          }
                      }
                      if (count == character1Check.Length - 1)//The newly generated character is the same as a previous one
                      {
                          tempMale = allCharacters[charNo].Gender;
                          allCharacters[charNo].Feature = new Features(randomNumber, tempMale);//The features of the newly generated character are regenerated
                      }
                      else
                      {
                          tempCharNumber--;
                      }
                  }
              }
          }
          return allCharacters;
      }

      public string[] SetUpFeatureCatagories(Characters character)//A function that returns a string array of all the features that make up a character
      {
          string[] allFeatureCatagories = new string[9];

          allFeatureCatagories[0] = character.Name;
          allFeatureCatagories[1] = character.Gender;
          allFeatureCatagories[2] = character.Feature.SkinColour;
          allFeatureCatagories[3] = character.Feature.EyeColour;
          allFeatureCatagories[4] = character.Feature.FacialHair;
          allFeatureCatagories[5] = character.Feature.SkinFeature;
          allFeatureCatagories[6] = character.Feature.Accessory;
          allFeatureCatagories[7] = character.Feature.Hair.Length;
          allFeatureCatagories[8] = character.Feature.Hair.Colour;

          return allFeatureCatagories;
      }

      public void AddValuestoCombo(ComboBox xLength, ComboBox yLength)
      {
        for (int amount = 2; amount < 9; amount++)
        {
            if (amount > 4)
            {
                xLength.Items.Add(amount);
            }
            if (amount < 5)
            {
                yLength.Items.Add(amount);
            }
        }
      }

      public void LandscapeSize(object sender, EventArgs e, Button playGame, Button instructions, FlowLayoutPanel f)
      {
        playerCharacterChosen = 'N';
        playGame.Dispose();
        instructions.Dispose();
        f.Dispose();

        FlowLayoutPanel flow = new FlowLayoutPanel();
        FlowLayoutPanelStyle(flow);

        Label askForAmount = new Label(); ///ADD ALL THIS TO A FLOW
        Label x = new Label();
        Label y = new Label();
        Button go = new Button();
        ComboBox xLength = new ComboBox();
        ComboBox yLength = new ComboBox();

        ControlStyle(askForAmount, "Please enter a board size", Color.White, Color.LightSkyBlue, 16);
        ControlStyle(x, "X: ", Color.White, Color.LightSkyBlue, 12);
        ControlStyle(y, "Y: ", Color.White, Color.LightSkyBlue, 12);
        ControlStyle(go, "Confirm", Color.LightSkyBlue, Color.White, 12);
        ControlStyle(xLength, "", Color.LightSkyBlue, Color.White, 12);
        ControlStyle(yLength, "", Color.LightSkyBlue, Color.White, 12);

        //ComboStyle(xLength, Color.LightSkyBlue, Color.White, 12, 30, 50, 550, 300);
        //ComboStyle(yLength, Color.LightSkyBlue, Color.White, 12, 30, 50, 640, 300);
        AddValuestoCombo(xLength, yLength);

        AddtoFlowLay(flow, askForAmount);
        AddtoFlowLay(flow, x);
        AddtoFlowLay(flow, y);
        AddtoFlowLay(flow, xLength);
        AddtoFlowLay(flow, yLength);
        AddtoFlowLay(flow, go);


        //LabelScale(askForAmount, width, 250);
        //LabelScale(X, width, 301);
        //LabelScale(y, width, x.Height + 10);
        //go.Size = new Size(85, 30);
        //go.Location = new Point(730, 300);
        go.Click += (senderValue, EventArgs) => { LoadGame(sender, EventArgs, go, askForAmount, xLength, yLength, x, y); };

      }

      public void LanSizeCheck()
      {
          Label announcement = new Label();
          //LabelStyle(announcement, "Please select valid co-ordinates to continue", Color.White, Color.Transparent, 12, 400, 20);
          PopUp falseAmount = new PopUp(announcement);
          falseAmount.Size = new Size(400, 100);
          falseAmount.Show();
      }

      public void LoadGame(object sender, EventArgs e, Button go, Label askForAmount, ComboBox xLength, ComboBox yLength, Label x, Label y)
      {
        if(xLength.SelectedItem == null || yLength.SelectedItem == null)
        {
          LanSizeCheck();
          return;
        }
        xAmount = Convert.ToInt16(xLength.SelectedItem);
        yAmount = Convert.ToInt32(yLength.SelectedItem);
        charAmount = xAmount * yAmount;
        go.Dispose();
        askForAmount.Dispose();
        xLength.Dispose();
        yLength.Dispose();
        x.Dispose();
        y.Dispose();
        Label instructionNotice = new Label();
        Label yourCharacter = new Label();
        Random randomNumber = new Random();
        bool floatingCharacters = false;
        //LabelStyle(instructionNotice, "Welcome, firstly please select a character.", Color.White, Color.LightSkyBlue, 12, width, 570);
        //LabelStyle(yourCharacter, "Your Character", Color.White, Color.LightSkyBlue, 12, width, 432);

        Characters[] boards = GeneratingCharacters(randomNumber, charAmount, floatingCharacters);//Generates the characters for the game
        Image[] drawnPictures = CreateImages(boards, charAmount);//Creates the images of those characters
        //PictureBox[] pictures = DrawBoard(boards, drawnPictures);//Draws the actual board
        //PlayerSelectCharacter(pictures, instructionNotice, yourCharacter, boards);//Allows the player to choose the character it wishes to be
      }
    }
}
