﻿using System;

namespace GuessWho
{
    public class Hairs
    {
        public string Length { get; set; }
        public string Colour { get; set; }

        public Hairs(Random RandomNumber)
        {
            string[] Lengths = { "Long Hair", "Short Hair", "Bald" };
            string[] Colours = { "Blonde Hair", "Light Brown Hair", "Dark Brown Hair", "Black Hair", "Blue Hair", "Ginger Hair", "Pink Hair", "White Hair", "Grey Hair", "Red Hair" };

            Length = Lengths[RandomNumber.Next(0, Lengths.Length)];

            if (Length == "Bald")
            {
                Colour = "None";
            }
            else
            {
                Colour = Colours[RandomNumber.Next(0, Colours.Length)];
            }
        }
    }
}
